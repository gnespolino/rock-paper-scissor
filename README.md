# Getting Started

### Requirements
This software was built with OpenJDK Java 11.

### Build


    ./gradlew clean build

### Start

    java -jar build/libs/rockpaperscissor-0.0.1-SNAPSHOT.jar
    
Open http://localhost:8080 in your favourite html5 compatible browser. 
