package org.nespolinux.ciklum.rockpaperscissor.game;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
class GlobalGameStats {
    private final long totalRoundPlayed;
    private final long totalWinsP1;
    private final long totalWinsP2;
    private final long totalDraws;
}
