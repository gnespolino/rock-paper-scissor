package org.nespolinux.ciklum.rockpaperscissor.game;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.nespolinux.ciklum.rockpaperscissor.model.Shape;

import java.time.Instant;

@RequiredArgsConstructor
public class GameCoreService {

    private final Player playerOne;
    private final Player playerTwo;

    public GameRoundResult playRound() {
        Shape shapeP1 = playerOne.chooseShape();
        Shape shapeP2 = playerTwo.chooseShape();
        return GameRoundResult.builder()
                .shapeP1(shapeP1)
                .shapeP2(shapeP2)
                .build();
    }

    @Builder
    @Data
    static class GameRoundResult {

        private final Shape shapeP1;
        private final Shape shapeP2;

        @Builder.Default
        private final Instant instant = Instant.now();

        @JsonProperty
        GameResult getGameResult() {
            return shapeP1.winsOver(shapeP2) ? GameResult.P1_WINS :
                    shapeP2.winsOver(shapeP1) ? GameResult.P2_WINS :
                            GameResult.TIE;
        }
    }

    enum GameResult {
        P1_WINS,
        P2_WINS,
        TIE
    }
}
