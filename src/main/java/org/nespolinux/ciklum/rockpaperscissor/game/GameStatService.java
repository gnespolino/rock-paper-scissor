package org.nespolinux.ciklum.rockpaperscissor.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class GameStatService {

    private final Map<String, Collection<GameCoreService.GameRoundResult>> gameStats = new HashMap<>();

    public String newGameById(String gameId) {
        gameStats.put(gameId, new LinkedList<>());
        return gameId;
    }

    public void assertGameExists(String gameId) {
        if (!gameStats.containsKey(gameId)) {
            throw new GameIdDoesntExistsException(gameId);
        }
    }

    public void logResult(String gameId, GameCoreService.GameRoundResult result) {
        gameStats.get(gameId).add(result);
    }

    public GlobalGameStats getGlobalStats() {
        Map<GameCoreService.GameResult, Long> frequencyMap = getStatsView().values().stream()
                .flatMap(Collection::stream)
                .map(GameCoreService.GameRoundResult::getGameResult)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return GlobalGameStats.builder()
                .totalRoundPlayed(frequencyMap.values().stream().reduce(0L, Long::sum))
                .totalDraws(Optional.ofNullable(frequencyMap.get(GameCoreService.GameResult.TIE)).orElse(0L))
                .totalWinsP1(Optional.ofNullable(frequencyMap.get(GameCoreService.GameResult.P1_WINS)).orElse(0L))
                .totalWinsP2(Optional.ofNullable(frequencyMap.get(GameCoreService.GameResult.P2_WINS)).orElse(0L))
                .build();
    }

    public Map<String, Collection<GameCoreService.GameRoundResult>> getStatsView() {
        Map<String, Collection<GameCoreService.GameRoundResult>> view = gameStats.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> Collections.unmodifiableCollection(entry.getValue())));
        return Collections.unmodifiableMap(view);
    }

    @RequiredArgsConstructor
    static class GameIdDoesntExistsException extends RuntimeException {
        @Getter
        private final String gameId;
    }
}
