package org.nespolinux.ciklum.rockpaperscissor.game;

import org.nespolinux.ciklum.rockpaperscissor.model.Shape;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GameConfiguration {

    @Bean
    GameCoreService gameCoreService() {
        return new GameCoreService(Shape::randomShape, () -> Shape.ROCK);
    }

}
