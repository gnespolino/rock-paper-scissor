package org.nespolinux.ciklum.rockpaperscissor.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class GameService {

    private final GameCoreService gameCoreService;
    private final GameStatService gameStatService;


    String startNewGameAndGetId() {
        return gameStatService.newGameById(UUID.randomUUID().toString());
    }

    GameCoreService.GameRoundResult playRound(String gameId) {
        gameStatService.assertGameExists(gameId);
        GameCoreService.GameRoundResult result = gameCoreService.playRound();
        gameStatService.logResult(gameId, result);
        return result;
    }


    public GlobalGameStats getGlobalStats() {
        return gameStatService.getGlobalStats();
    }
}
