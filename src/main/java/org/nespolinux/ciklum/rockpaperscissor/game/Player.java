package org.nespolinux.ciklum.rockpaperscissor.game;

import org.nespolinux.ciklum.rockpaperscissor.model.Shape;

interface Player {
    Shape chooseShape();
}
