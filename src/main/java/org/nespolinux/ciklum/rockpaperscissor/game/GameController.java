package org.nespolinux.ciklum.rockpaperscissor.game;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("game")
@RequiredArgsConstructor
class GameController {

    private final GameService gameService;

    @GetMapping("{gameId}/next")
    GameCoreService.GameRoundResult nextRound(@PathVariable String gameId) {
        return gameService.playRound(gameId);
    }

    @PostMapping("new")
    String newGame() {
        return gameService.startNewGameAndGetId();
    }

    @GetMapping("stats")
    GlobalGameStats stats() {
        return gameService.getGlobalStats();
    }

}
