package org.nespolinux.ciklum.rockpaperscissor.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public enum Shape {
    PAPER,
    SCISSORS,
    ROCK;

    private static final List<Shape> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();

    private static final Map<Shape, Shape> WINS_MAP = Collections.unmodifiableMap(
            Map.of(PAPER, ROCK,
                   SCISSORS, PAPER,
                   ROCK, SCISSORS));

    public static Shape randomShape() {
        return VALUES.get(ThreadLocalRandom.current().nextInt(SIZE));
    }

    public boolean winsOver(Shape another) {
        return WINS_MAP.get(this).equals(another);
    }
}
