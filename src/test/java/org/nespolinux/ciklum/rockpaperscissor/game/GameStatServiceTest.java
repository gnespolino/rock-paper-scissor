package org.nespolinux.ciklum.rockpaperscissor.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nespolinux.ciklum.rockpaperscissor.model.Shape;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameStatServiceTest {

    private final static String A_RANDOM_STRING = "arandomstring";
    private final static String ANOTHER_RANDOM_STRING = "anotherrandomstring";

    private final static GameCoreService.GameRoundResult A_GAME_ROUND_RESULT = GameCoreService.GameRoundResult.builder()
            .shapeP1(Shape.ROCK)
            .shapeP2(Shape.PAPER)
            .build();

    private final static GameCoreService.GameRoundResult ANOTHER_GAME_ROUND_RESULT = GameCoreService.GameRoundResult.builder()
            .shapeP1(Shape.SCISSORS)
            .shapeP2(Shape.PAPER)
            .build();

    private final static GameCoreService.GameRoundResult A_DRAW_GAME_ROUND_RESULT = GameCoreService.GameRoundResult.builder()
            .shapeP1(Shape.PAPER)
            .shapeP2(Shape.PAPER)
            .build();

    private GameStatService gameStatService;

    @BeforeEach
    void setup() {
        gameStatService = new GameStatService();
        gameStatService.newGameById(A_RANDOM_STRING);
    }

    @Test
    void newGameRoundTest() {
        assertThat(gameStatService.getStatsView()).containsKeys(A_RANDOM_STRING);
    }

    @Test
    void assertGameExists_throws_exception() {
        GameStatService.GameIdDoesntExistsException exception = assertThrows(GameStatService.GameIdDoesntExistsException.class, () -> gameStatService.assertGameExists(ANOTHER_RANDOM_STRING));
        assertThat(exception.getGameId()).isEqualTo(ANOTHER_RANDOM_STRING);
    }

    @Test
    void assertGameExists_doesnt_throw_exception() {
        gameStatService.assertGameExists(A_RANDOM_STRING);
    }

    @Test
    void logGameResultTest() {
        gameStatService.logResult(A_RANDOM_STRING, A_GAME_ROUND_RESULT);
        gameStatService.logResult(A_RANDOM_STRING, ANOTHER_GAME_ROUND_RESULT);
        assertThat(gameStatService.getStatsView().get(A_RANDOM_STRING).containsAll(List.of(A_GAME_ROUND_RESULT, ANOTHER_GAME_ROUND_RESULT)));
        assertThat(gameStatService.getStatsView().get(A_RANDOM_STRING)).hasSize(2);
    }

    @Test
    void globalStat_test() {
        gameStatService.logResult(A_RANDOM_STRING, A_GAME_ROUND_RESULT);
        gameStatService.logResult(A_RANDOM_STRING, ANOTHER_GAME_ROUND_RESULT);
        gameStatService.logResult(A_RANDOM_STRING, A_DRAW_GAME_ROUND_RESULT);
        assertThat(gameStatService.getGlobalStats().getTotalRoundPlayed()).isEqualTo(3);
        assertThat(gameStatService.getGlobalStats().getTotalWinsP1()).isEqualTo(1);
        assertThat(gameStatService.getGlobalStats().getTotalWinsP2()).isEqualTo(1);
        assertThat(gameStatService.getGlobalStats().getTotalDraws()).isEqualTo(1);
    }
}
