package org.nespolinux.ciklum.rockpaperscissor.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.PAPER;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.ROCK;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.SCISSORS;

public class ShapeTest {

    @Test
    public void testWinsOver() {
        assertTrue(SCISSORS.winsOver(PAPER));
        assertFalse(SCISSORS.winsOver(ROCK));
        assertFalse(SCISSORS.winsOver(SCISSORS));

        assertTrue(PAPER.winsOver(ROCK));
        assertFalse(PAPER.winsOver(SCISSORS));
        assertFalse(PAPER.winsOver(PAPER));

        assertTrue(ROCK.winsOver(SCISSORS));
        assertFalse(ROCK.winsOver(PAPER));
        assertFalse(ROCK.winsOver(ROCK));
    }
}
