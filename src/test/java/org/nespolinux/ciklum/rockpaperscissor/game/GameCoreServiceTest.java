package org.nespolinux.ciklum.rockpaperscissor.game;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;
import org.nespolinux.ciklum.rockpaperscissor.model.Shape;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.nespolinux.ciklum.rockpaperscissor.game.GameCoreService.GameResult.P1_WINS;
import static org.nespolinux.ciklum.rockpaperscissor.game.GameCoreService.GameResult.P2_WINS;
import static org.nespolinux.ciklum.rockpaperscissor.game.GameCoreService.GameResult.TIE;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.PAPER;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.ROCK;
import static org.nespolinux.ciklum.rockpaperscissor.model.Shape.SCISSORS;

public class GameCoreServiceTest {

    private final static Collection<Triple<Shape, Shape, GameCoreService.GameResult>> TEST_DATA = List.of(
            Triple.of(ROCK, ROCK, TIE),
            Triple.of(SCISSORS, SCISSORS, TIE),
            Triple.of(PAPER, PAPER, TIE),

            Triple.of(ROCK, PAPER, P2_WINS),
            Triple.of(ROCK, SCISSORS, P1_WINS),

            Triple.of(SCISSORS, ROCK, P2_WINS),
            Triple.of(SCISSORS, PAPER, P1_WINS),

            Triple.of(PAPER, SCISSORS, P2_WINS),
            Triple.of(PAPER, ROCK, P1_WINS));

    @Test
    public void playRoundTest() {
        TEST_DATA.forEach(this::assertTestCaseIsValid);
    }

    private void assertTestCaseIsValid(Triple<Shape, Shape, GameCoreService.GameResult> testCase) {
        GameCoreService gameCoreService = new GameCoreService(testCase::getLeft, testCase::getMiddle);
        assertThat(gameCoreService.playRound().getGameResult()).isEqualTo(testCase.getRight());
    }

}
